<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'title',
        'slug',
        'descriptionOne',
        'descriptionTwo',
        'image',
    ];

    public function storeArticle($request)
    {
            $product = self::create([
                'category_id' => $request->category_id,
                'title' => $request->category_id,
                'slug' => "./article/",
                'descriptionOne' => $request->descriptionOne,
                'descriptionTwo'=> $request->descriptionTwo,
                'image'=> $request->image,
            ]);

            return $product;
    }

    public function updateArticle($request)
    {
        $article = self::update($request->all());

        return $article;
    }
   

    public function category()
    {
        return $this->hasOne(Category::class);
    }
}
