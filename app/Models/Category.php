<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name'
    ];
    
    public function storeCategory($request)
    {
            $category = self::create($request->all());

            return $category;
    }

    public function updateCategory($request)
    {
        $category = self::update($request->all());

        return $category;
    }
   
  
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
