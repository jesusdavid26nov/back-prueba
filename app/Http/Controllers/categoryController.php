<?php

namespace App\Http\Controllers;

use App\Http\Requests\categoryStoreRequest;
use App\Http\Requests\categoryUpdateRequest;
use App\Models\Category;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class categoryController extends Controller
{
    use ApiResponser;
    
    public function __construct()
    {
        $this->middleware('client');
    }

    public function index()
    {
        $data = Category::all();
        return $this->successResponse($data, 200);
    }

    public function store(categoryStoreRequest $request, Category $category)
    {
        $data = $category->storeCategory($request);
        return $this->successResponse($data, 200);
    }

    public function update(categoryUpdateRequest $request, $id)
    {   
        $data = Category::find($id);
        $data->updateCategory($request);
        return $this->successResponse($data, 200);
    }

    public function destroy($id)
    {
            $data = Category::find($id);
            if ($data) {
                $data->delete();
                return $this->successResponse($data, 200);
            }else {
                return $this->errorResponse('Not Found', 404);
            };
      
    }
}
