<?php

namespace App\Http\Controllers;

use App\Http\Requests\storeArticleRequest;
use App\Models\Article;
use App\Models\Category;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class articleController extends Controller
{
    use ApiResponser;

    public function __construct()
    {
        $this->middleware('client');
    }

    public function index()
    {
        $data = Article::addSelect(['category' => Category::select('name')
        ->whereColumn('articles.category_id', 'categories.id')
        ->orderByDesc('id')
        ->limit(1)
        ])->get();
        return $this->successResponse($data, 200);
    }

    public function store(storeArticleRequest $request, Article $article)
    {
        $data = $article->storeArticle($request);
        return $this->successResponse($data, 200);
    }


    public function show($id)
    {
        $data = Article::find($id);
       
        if ($data) {
            $category = Category::find($data->category_id);

            $data = [
                'category' => $category->name,
                'title' => $data->title,
                'slug' => $data->slug,
                'descriptionOne' => $data->descriptionOne,
                'descriptionTwo' => $data->descriptionOne,
                'image' => $data->image,     
            ];

            return $this->successResponse($data, 200);
        }else {
            return $this->errorResponse('Not Found', 404);
        };
    }

    public function update(Request $request, $id)
    {   
        $data = Article::find($id);
        $data->updateArticle($request);
        return $this->successResponse($data, 200);
    }

    public function destroy($id)
    {
            $article = Article::find($id);
            if ($article) {
                $article->delete();
                return $this->successResponse($article, 200);
            }else {
                return $this->errorResponse('Not Found', 404);
            };
      
    }
}
