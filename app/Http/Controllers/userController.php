<?php

namespace App\Http\Controllers;

use App\Http\Requests\storeUserRequest;
use App\Http\Requests\updateUserRequest;
use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;

class userController extends Controller
{
    use ApiResponser;
    
    public function index()
    {
        $data = User::all();
        return $this->successResponse($data, 200);
    }

    public function update(updateUserRequest $request, $id)
    {   
        $data = User::find($id);
        $data->updateUser($request);
        return $this->successResponse($data, 200);;
    }

    public function destroy($id)
    {
            $data = User::find($id);
            if ($data) {
                $data->delete();
                return $this->successResponse($data, 200);
            }else {
                return $this->errorResponse('Not Found', 404);
            };
      
    }
}
