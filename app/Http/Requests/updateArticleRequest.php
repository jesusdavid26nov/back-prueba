<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'number',
            'title'=> 'min:5|max:60|string',
            'slug'=> 'unique:articles,slug, '.$this->route('articles').'|min:5|max:255|string',
            'descriptionOne' => 'min:5|max:255|string', 
            'descriptionTwo' => 'min:5|max:255|string',
            'image'=> 'min:5|max:255|string',
        ];
    }
    public function messages()
    {
        return [
            'category_id.number' => 'It has to be number type',
            'title.max' => 'maximum is 60 characters',
            'title.min' => 'minimum is 5 characters',
            'title.string' => 'It has to be string type',
            'slug.max' => 'maximum is 255 characters',
            'slug.min' => 'minimum is 5 characters',
            'slug.string' => 'It has to be string type',
            'descriptionOne.max' => 'maximum is 255 characters',
            'descriptionOne.min' => 'minimum is 5 characters',
            'descriptionOne.string' => 'It has to be string type',
            'descriptionTwo.max' => 'maximum is 255 characters',
            'descriptionTwo.min' => 'minimum is 5 characters',
            'descriptionTwo.string' => 'It has to be string type',
            'image.max' => 'maximum is 255 characters',
            'image.min' => 'minimum is 5 characters',
            'image.string' => 'It has to be string type',
            ];
    }
}
