<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storeArticleRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'category_id' => 'required|integer',
            'title'=> 'required|min:5|max:60|string',
            'descriptionOne' => 'required|min:5|max:255|string', 
            'descriptionTwo' => 'required|min:5|max:255|string',
            'image'=> 'required|min:5|max:255|string',
        ];
    }
    public function messages()
    {
        return [
        'category_id.required' => 'the name is required',
        'category_id.integer' => 'It has to be number type',
        'title.required' => 'the title is required',
        'title.max' => 'maximum is 60 characters',
        'title.min' => 'minimum is 5 characters',
        'title.string' => 'It has to be string type',
        'descriptionOne.required' => 'the descriptionOne is required',
        'descriptionOne.max' => 'maximum is 255 characters',
        'descriptionOne.min' => 'minimum is 5 characters',
        'descriptionOne.string' => 'It has to be string type',
        'descriptionTwo.required' => 'the descriptionTwo is required',
        'descriptionTwo.max' => 'maximum is 255 characters',
        'descriptionTwo.min' => 'minimum is 5 characters',
        'descriptionTwo.string' => 'It has to be string type',
        'image.required' => 'the image is required',
        'image.max' => 'maximum is 255 characters',
        'image.min' => 'minimum is 5 characters',
        'image.string' => 'It has to be string type',
        ];
    }
}
