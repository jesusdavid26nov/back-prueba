<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class updateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    
        public function rules()
    {
        return [
            'name' => 'max:60|string',
            'email'=> 'unique:users|max:255|string|email',
            'phone'=> 'max:11|string',
            'password' => 'min:8|max:10|string', 
            'email' => 'unique:users,email, '.$this->route('users').'|max:255|string|email',
        ];
    }
    public function messages()
    {
        return [
            'name.max' => 'maximum is 60 characters',
            'name.string' => 'It has to be string type ',
            'email.unique' => 'is already in the database',
            'email.email' => 'it has to be email',
            'email.string' => 'it has to be email',
            'password.min' => 'minimum is 8 characters',
            'password.max' => 'maximum is 10 characters',
            'password.string' => 'It has to be string type ',
            'phone.string' => 'It has to be string type',
            'phone.max' => 'maximum is 10 characters',
        ];
    }
}
