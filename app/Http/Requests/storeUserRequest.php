<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class storeUserRequest extends FormRequest
{
 
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'name' => 'required|max:60|string',
            'email'=> 'required|unique:users|max:255|string|email',
            'phone'=> 'required|max:11|string',
            'password' => 'required|min:8|max:10|string', 
  
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'the name is required',
            'name.max' => 'maximum is 60 characters',
            'name.string' => 'It has to be string type ',
            'email.required' => 'the email is required',
            'email.unique' => 'is already in the database',
            'email.email' => 'it has to be email',
            'email.string' => 'it has to be email',
            'password.min' => 'minimum is 8 characters',
            'password.required' => 'the password is required',
            'password.max' => 'maximum is 10 characters',
            'password.string' => 'It has to be string type ',
            'phone.required' => 'the phone is required',
            'phone.string' => 'It has to be string type',
            'phone.max' => 'maximum is 10 characters',
        ];
    }
}
