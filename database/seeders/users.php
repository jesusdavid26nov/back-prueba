<?php

namespace Database\Seeders;

use Illuminate\Support\Str;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admins = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.co',
            'password' => Hash::make('admin123'),
            'phone' => '3020457890',
            'role' => 'admin'
        ]);

    }
}
